"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getLanguages_1 = require("./getLanguages");
exports.getLanguages = getLanguages_1.default;
var translate_1 = require("./translate");
exports.translate = translate_1.default;
var translateDictionary_1 = require("./translateDictionary");
exports.translateDictionary = translateDictionary_1.default;
var api_1 = require("./api");
exports.API = api_1.default;
