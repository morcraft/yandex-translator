import request = require('request-promise');
export default function getLanguages(API: string, apiKey: string, displayLanguage: string): request.RequestPromise;
