"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var translate_1 = require("./translate");
function translateDictionary(API, apiKey, dictionary, translateTo) {
    return new Promise(function (resolve, reject) {
        var translatedDictionary = {};
        Object.keys(dictionary).forEach(function (entry) {
            translate_1.default(API, apiKey, translateTo, dictionary[entry])
                .then(function (response) {
                try {
                    var JSONContent = JSON.parse(response);
                    translatedDictionary[entry] = JSONContent.text[0];
                }
                catch (error) {
                    reject(error);
                }
                if (Object.keys(translatedDictionary).length == Object.keys(dictionary).length)
                    resolve(translatedDictionary);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    });
}
exports.default = translateDictionary;
