"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request = require("request-promise");
var qs = require("qs");
function translate(API, apiKey, translateTo, text) {
    var queryArguments = qs.stringify({
        key: apiKey,
        lang: translateTo,
        text: text
    });
    return request(API + "/translate?" + queryArguments);
}
exports.default = translate;
