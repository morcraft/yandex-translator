import request = require('request-promise');
export default function translate(API: string, apiKey: string, translateTo: string, text: string): request.RequestPromise;
