"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request = require("request-promise");
var qs = require("qs");
function getLanguages(API, apiKey, displayLanguage) {
    var queryArguments = qs.stringify({
        key: apiKey,
        ui: displayLanguage
    });
    return request(API + "/getLangs?" + queryArguments);
}
exports.default = getLanguages;
