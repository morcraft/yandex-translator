import Dictionary from './dictionary';
export default function translateDictionary(API: string, apiKey: string, dictionary: Dictionary, translateTo: string): Promise<Dictionary>;
