import request = require('request-promise')
import qs = require('qs')

export default function getLanguages(API: string, apiKey: string, displayLanguage: string){
    const queryArguments = qs.stringify({
        key: apiKey,
        ui: displayLanguage
    })

    return request(`${API}/getLangs?${queryArguments}`)
}