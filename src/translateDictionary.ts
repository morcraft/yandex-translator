import request = require('request-promise')
import qs = require('qs')
import Dictionary from './dictionary'
import translate from './translate'
import TranslationResponse from './translationResponse'

export default function translateDictionary(API: string, apiKey: string, dictionary: Dictionary, translateTo: string){
    return new Promise<Dictionary>((resolve, reject) => {
        const translatedDictionary = {}
        Object.keys(dictionary).forEach(entry => {
            translate(API, apiKey, translateTo, dictionary[entry])
            .then(response => {
                try{
                    const JSONContent: TranslationResponse = JSON.parse(response)
                    translatedDictionary[entry] = JSONContent.text[0]
                }
                catch(error){ reject(error) }
                if(Object.keys(translatedDictionary).length == Object.keys(dictionary).length)
                    resolve(translatedDictionary)
            })
            .catch(error => {
                reject(error)
            })
        })
    })
}