export default interface TranslationResponse{
    code: string
    lang: string
    text: string[]
}