import Dictionary from './dictionary'
import getLanguages from './getLanguages'
import translate from './translate'
import translateDictionary from './translateDictionary'
import TranslationResponse from './translationResponse'
import API from './api'

export { API, Dictionary, getLanguages, translate, translateDictionary, TranslationResponse }