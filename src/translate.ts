import request = require('request-promise')
import qs = require('qs')

export default function translate(API: string, apiKey: string, translateTo: string, text: string){
    const queryArguments = qs.stringify({
        key: apiKey,
        lang: translateTo,
        text: text
    })

    return request(`${API}/translate?${queryArguments}`)
}